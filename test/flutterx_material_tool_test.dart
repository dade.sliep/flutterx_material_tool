import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutterx_material_tool/flutterx_material_tool.dart';
import 'package:flutterx_material_tool/src/model/colors.dart';
import 'package:flutterx_material_tool/src/model/material_palette.dart';

void main() {
  test('Test tool', () {
    final data = MaterialPalette.primary.generate(Colors.blue.rgb);
    assert(data.data.value == Colors.blue.value);
    assert(data.swatch[4] == Colors.blue[500]);
    assert(data.data[500] == Colors.blue[500]);
    assert(data.data[400] == Colors.blue[400]);
    assert(data.data[900] == Colors.blue[900]);
    assert(data.data[50] == Colors.blue[50]);
    assert(data + 1 == Colors.blue[400]);
    assert(MaterialPalette.accent.generate(Colors.redAccent.rgb).inputIndex == 2);
    assert(data - 10 == Colors.blue[900]);

    final palette = MaterialPalette.primary;
    final swatch = palette.generate(const Color(0xff607D8B).rgb).data;
    swatch.printDescription(palette.shades);
    for (final primary in palette.palettes) {
      final swatch = palette.generate(primary[palette.primaryShadeIndex].toLCHColor().toRGBColor()).data;
      print('---');
      for (final shade in palette.shades) {
        final description = '${shade.toString().padLeft(3, '0')}: ${swatch[shade]!.hex}';
        print(
            '    $description ${swatch[shade] == primary[palette.shades.indexOf(shade)].toLCHColor().toRGBColor().toColor() ? 'Y' : 'X'}');
      }
    }
  });
}

extension ColorExt on Color {
  String get hex => '#${value.toRadixString(16).substring(2).toUpperCase()}';
}

extension ColorSwatchExt on ColorSwatch<int> {
  void printDescription(List<int> shades, [String? label]) {
    var indent = '';
    if (label != null) {
      print(label);
      indent = '    ';
    }
    for (final shade in shades) {
      final description = '${shade.toString().padLeft(3, '0')}: ${this[shade]!.hex}';
      print('$indent$description');
    }
  }
}
