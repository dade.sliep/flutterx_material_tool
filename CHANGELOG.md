## 1.1.0

* Initial stable release

## 1.0.5-dev

* Fix generated data order.

## 1.0.4-dev

* Fixes.

## 1.0.3-dev

* Add theme utility.

## 1.0.2-dev

* API improvements.

## 1.0.1-dev

* Optimized dependencies.

## 1.0.0-dev

* Initial release.
