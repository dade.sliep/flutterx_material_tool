import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart' hide DialogRoute;
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutterx_application/flutterx_application.dart';
import 'package:flutterx_material_tool/flutterx_material_tool.dart';
import 'package:flutterx_utils/flutterx_utils.dart';

class ExportDialog extends StatelessWidget {
  static final DialogRoute<void> route = DialogRoute.alertBuilder('export',
      title: 'colors.xml',
      neutralAction: AlertDialogAction(
          text: 'COPY ALL',
          onAction: (context, outArgs) {
            Clipboard.setData(ClipboardData(text: outArgs['content']));
            final messenger = ScaffoldMessenger.of(context);
            messenger.hideCurrentSnackBar();
            messenger.showSnackBar(const SnackBar(content: Text('colors.xml copied to clipboard')));
          }),
      positiveAction: AlertDialogAction(text: 'CLOSE', onAction: (context, outArgs) => null),
      insetPadding: const EdgeInsets.all(24.0),
      contentPadding: const EdgeInsets.fromLTRB(0, 20.0, 0, 24.0),
      scrollable: true, builder: (context, args, outArgs) {
    outArgs['content'] = args['content'];
    return ExportDialog._(content: args['content']);
  });

  final String content;

  const ExportDialog._({Key? key, required this.content}) : super(key: key);

  static Future<void> open(BuildContext context, MaterialPalette<ColorSwatch<int>> palette, RGBColor color) {
    final result = <String>[];
    for (final variant in MaterialVariant.values) {
      final shades = palette.generate(color, variant: variant);
      final variantName = describeEnum(variant).lowerCamelCase;
      final shadeName = palette.shades.iterator;
      for (final shade in shades.swatch) {
        shadeName.moveNext();
        final colorName = '${variantName}_${shadeName.current}';
        result.add('<color name="$colorName">#${shade.hexString}</color>');
      }
    }
    final xml = '<!--?xml version="1.0" encoding="UTF-8"?-->\n<resources>\n    ${result.join('\n    ')}\n</resources>';
    return route.open(context, args: {'content': xml});
  }

  @override
  Widget build(BuildContext context) => SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: SelectableText(content, style: Theme.of(context).textTheme.caption));
}
