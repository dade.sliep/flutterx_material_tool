import 'package:example/shades_builder.dart';
import 'package:flutter/material.dart';

class ShadesIndicator extends StatelessWidget {
  final List<int> shades;
  final EdgeInsets margin;

  const ShadesIndicator({
    Key? key,
    required this.shades,
    this.margin = const EdgeInsets.symmetric(horizontal: defaultPadding),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => ShadesBuilder(
      shades: shades,
      margin: margin,
      builder: (context, shades, length, size, hoverSize, margin, padding) => Padding(
          padding: margin + EdgeInsets.symmetric(horizontal: padding),
          child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: shades.reversed
                  .map((shade) => Container(
                      width: size,
                      height: 24,
                      alignment: Alignment.center,
                      child: Text(shade.toString(), style: Theme.of(context).textTheme.caption)))
                  .toList(growable: false))));
}
