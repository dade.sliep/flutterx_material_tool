import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart' hide DialogRoute;
import 'package:flutter/widgets.dart';
import 'package:flutterx_application/flutterx_application.dart';
import 'package:url_launcher/url_launcher.dart';

class AboutDialog extends StatelessWidget {
  static const _appName = 'Material Tool';
  static const _appVersion = '1.0.0';
  static final DialogRoute<void> route = DialogRoute.alertBuilder('about',
      neutralAction: AlertDialogAction(
          text: 'LICENSES',
          onAction: (context, outArgs) => showLicensePage(
              context: context,
              applicationName: _appName,
              applicationVersion: _appVersion,
              applicationLegalese: 'Source code available on gitlab.com',
              applicationIcon: const Icon(Icons.palette)),
          onResult: null),
      positiveAction: AlertDialogAction(text: 'CLOSE', onAction: (context, outArgs) => null),
      insetPadding: const EdgeInsets.all(24.0),
      contentPadding: const EdgeInsets.fromLTRB(16, 20.0, 16, 24.0),
      builder: (context, args, outArgs) => const AboutDialog._());

  const AboutDialog._({Key? key}) : super(key: key);

  static Future<void> open(BuildContext context) => route.open(context);

  @override
  Widget build(BuildContext context) =>
      Column(mainAxisSize: MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
        Row(children: [
          const Icon(Icons.palette),
          const SizedBox(width: 8),
          Text(_appName, style: Theme.of(context).textTheme.headline5),
        ]),
        Text(_appVersion, style: Theme.of(context).textTheme.bodyText2),
        const SizedBox(height: 18),
        RichText(
            text: TextSpan(style: Theme.of(context).textTheme.caption, children: [
          const TextSpan(text: 'This project is inspired by the official Material '),
          _link(context, 'https://material.io/inline-tools/color/', label: 'Color Tool\n\n'),
          const TextSpan(text: 'Source code of this application is available on '),
          _link(context, 'https://gitlab.com/dade.sliep/flutterx_material_tool/-/tree/master/example', label: 'gitlab'),
          const TextSpan(text: '.\n\n'),
          const TextSpan(text: 'Powered by flutterx libraries:'),
          ..._gitlabLink(context, 'flutterx_application'),
          ..._gitlabLink(context, 'flutterx_color_picker'),
          ..._gitlabLink(context, 'flutterx_live_data'),
          ..._gitlabLink(context, 'flutterx_material_tool'),
          ..._gitlabLink(context, 'flutterx_utils'),
        ])),
      ]);

  Iterable<TextSpan> _gitlabLink(BuildContext context, String name) =>
      [const TextSpan(text: '\n• '), _link(context, 'https://gitlab.com/dade.sliep/$name', label: name)];

  TextSpan _link(BuildContext context, String link, {String? label}) => TextSpan(
      style: TextStyle(color: Theme.of(context).colorScheme.secondary),
      text: label ?? link,
      recognizer: TapGestureRecognizer()..onTap = () => launch(link));
}
