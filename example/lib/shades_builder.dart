import 'dart:math';

import 'package:flutter/material.dart';

const double defaultPadding = 12;

class ShadesBuilder extends StatelessWidget {
  final List<int> shades;
  final double hoverSizeFactor;
  final EdgeInsets margin;
  final Widget Function(BuildContext context, List<int> shades, int length, double size, double hoverSize,
      EdgeInsets margin, double padding) builder;

  const ShadesBuilder({
    Key? key,
    required this.shades,
    this.hoverSizeFactor = 1.2,
    this.margin = const EdgeInsets.symmetric(horizontal: defaultPadding),
    required this.builder,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => LayoutBuilder(builder: (context, constraints) {
        final length = shades.length;
        final size = (constraints.maxWidth - margin.horizontal) / (length - 1 + max(1, hoverSizeFactor));
        final hoverSize = size * hoverSizeFactor;
        final padding = (hoverSize - size) / 2;
        return builder(context, shades, length, size, hoverSize, margin, padding);
      });
}
