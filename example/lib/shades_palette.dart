import 'package:example/shades_builder.dart';
import 'package:flutter/material.dart';
import 'package:flutterx_material_tool/flutterx_material_tool.dart';
import 'package:flutterx_utils/flutterx_utils.dart';

class ShadesPalette extends StatefulWidget {
  final MaterialVariant variant;
  final PaletteData data;
  final EdgeInsets margin;
  final void Function(MaterialVariant variant, PaletteData data, int index) onColor;

  ShadesPalette({
    Key? key,
    required MaterialPalette<ColorSwatch<int>> palette,
    required this.variant,
    required RGBColor input,
    this.margin = const EdgeInsets.symmetric(horizontal: defaultPadding),
    required this.onColor,
  })  : data = palette.generate(input, variant: variant),
        super(key: key);

  @override
  State<ShadesPalette> createState() => _ShadesPaletteState();
}

class _ShadesPaletteState extends State<ShadesPalette> {
  final _keys = <Object, GlobalKey>{}; // using global key cause children position always change
  late Offset _localPosition;
  int? _hoverIndex;

  @override
  Widget build(BuildContext context) => ShadesBuilder(
      shades: widget.data.palette.shades,
      margin: widget.margin,
      builder: (context, shades, length, size, hoverSize, margin, padding) {
        margin += EdgeInsets.symmetric(horizontal: padding, vertical: padding * 2);
        final children = <Widget>[];
        Widget? selectedItem;
        for (var i = 0; i < length; i++) {
          final key = _keys[Pair(widget.variant, i)] ??= GlobalKey();
          final color = widget.data.swatch[i];
          final selected = widget.data.inputIndex == i;
          final hovered = _hoverIndex == i;
          final item = Positioned(
              left: margin.left + size * i + (hovered ? -padding : 0),
              top: margin.top + (hovered ? 0 : padding),
              child: _buildIndicator(key, size, hoverSize, color, selected, hovered));
          selected ? selectedItem = item : children.add(item);
        }
        if (selectedItem != null) children.add(selectedItem);

        void _onPan(Offset localPosition, bool down, [bool shift = true]) {
          final index = (localPosition.dx ~/ size).clamp(0, length - 1);
          final newIndex = down ? index : null;
          if (_hoverIndex != newIndex) setState(() => _hoverIndex = newIndex);
          widget.onColor(shift ? widget.variant : MaterialVariant.primary, widget.data, index);
        }

        final detector = Padding(
            padding: margin,
            child: GestureDetector(
                onPanDown: (update) => _localPosition = update.localPosition,
                onTap: () => _onPan(_localPosition, false, false),
                onPanEnd: (_) => _onPan(_localPosition, false),
                onPanCancel: () => _onPan(_localPosition, false),
                onPanUpdate: (update) => _onPan(_localPosition = update.localPosition, true)));
        children.add(Positioned.fill(child: detector));
        return SizedBox(
            height: size + margin.vertical,
            child: Stack(fit: StackFit.expand, alignment: Alignment.center, children: children));
      });

  Widget _buildIndicator(Key key, double size, double hoverSize, Color color, bool selected, bool hovered) => Material(
      key: key,
      shape: selected ? const CircleBorder() : const RoundedRectangleBorder(),
      color: color,
      animationDuration: kThemeChangeDuration * 2,
      elevation: hovered ? 4 : 1,
      child: SizedBox.fromSize(size: Size.square(hovered ? hoverSize : size)));
}
