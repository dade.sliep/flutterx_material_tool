import 'package:example/shades_builder.dart';
import 'package:flutter/material.dart';

class ShadesTitle extends StatelessWidget {
  final List<int> shades;
  final String title;
  final EdgeInsets margin;

  const ShadesTitle({
    Key? key,
    required this.shades,
    required this.title,
    this.margin = const EdgeInsets.only(left: defaultPadding, top: 16, right: defaultPadding),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => ShadesBuilder(
      shades: shades,
      margin: margin,
      builder: (context, shades, length, size, hoverSize, margin, padding) => Padding(
          padding: margin + EdgeInsets.symmetric(horizontal: padding),
          child: Text(title, style: Theme.of(context).textTheme.subtitle2)));
}
