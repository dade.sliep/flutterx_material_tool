# flutterx_material_tool

Reverse engineered Google material tool allows to generate material palette from primary color

## Import

Import the library like this:

```dart
import 'package:flutterx_material_tool/flutterx_material_tool.dart';
```

## Usage

Check the documentation in the desired source file of this library