import 'dart:math' as math;

import 'package:flutterx_material_tool/src/model/colors.dart';
import 'package:flutterx_material_tool/src/model/indexed_palette.dart';
import 'package:flutterx_material_tool/src/model/material_palette.dart';
import 'package:flutterx_material_tool/src/utils.dart';

/// Generate swatch of [data] from [input]
Iterable<RGBColor> generateSwatch(RGBColor input, MaterialPalette data) sync* {
  final inputLAB = input.toLABColor();
  final ip = IndexedPalette.of(inputLAB, data.palettes);
  final index = ip.index;
  final palette = ip.palette.map((e) => e.toLCHColor()).toList(growable: false);
  final current = palette[index];
  final inputLCH = inputLAB.toLCHColor();
  final lowChroma = palette[data.primaryShadeIndex].chroma < 30;
  final lightness = current.lightness - inputLCH.lightness;
  final saturation = current.chroma - inputLCH.chroma;
  final hue = current.hue - inputLCH.hue;
  final lightnessFactor = _lightnessFactors[index];
  final chromaFactor = _chromaFactors[index];
  var maxLightness = 100.0;
  for (var i = 0; i < palette.length; i++) {
    final item = palette[i];
    if (item == current) {
      maxLightness = math.max(inputLCH.lightness - 1.7, .0);
      yield input;
    } else {
      final result = LCHColor.fromALCH(
          input.alpha,
          math.min(item.lightness - _lightnessFactors[i] / lightnessFactor * lightness, maxLightness).clamp(.0, 100.0),
          (lowChroma
                  ? item.chroma - saturation
                  : item.chroma - saturation * math.min(_chromaFactors[i] / chromaFactor, 1.25))
              .clamp(.0, double.infinity),
          (item.hue - hue).clampDegrees());
      maxLightness = math.max(result.lightness - 1.7, 0);
      yield result.toRGBColor();
    }
  }
}

const _lightnessFactors = [
  2.048875457,
  5.124792061,
  8.751659557,
  12.07628774,
  13.91449542,
  15.92738893,
  15.46585818,
  15.09779227,
  15.13738673,
  15.09818372,
];

const _chromaFactors = [
  1.762442714,
  4.213532634,
  7.395827458,
  11.07174158,
  13.89634504,
  16.37591477,
  16.27071136,
  16.54160806,
  17.35916727,
  19.88410864,
];
