import 'package:flutter/material.dart';
import 'package:flutterx_material_tool/flutterx_material_tool.dart';

/// Generate [ColorScheme] from input parameters
/// Non provided parameters are automatically generated from provided ones
ColorScheme generateScheme({
  Brightness? brightness,
  required Color primary,
  Color? primaryVariant,
  Color? secondary,
  Color? secondaryVariant,
  MaterialVariant secondaryFromPrimary = MaterialVariant.complementary,
  Color? surface,
  Color background = Colors.white,
  Color error = const Color(0xffb00020),
  Color onDark = Colors.white,
  Color onLight = Colors.black,
  Color? onPrimary,
  Color? onSecondary,
  Color? onSurface,
  Color? onBackground,
  Color? onError,
}) {
  brightness ??= ThemeData.estimateBrightnessForColor(background);
  PaletteData<MaterialColor>? _primaryPalette;
  PaletteData<MaterialColor> primaryPalette() => _primaryPalette ??= MaterialPalette.primary.generate(primary.rgb);

  PaletteData<MaterialAccentColor>? _secondaryPalette;
  PaletteData<MaterialAccentColor> secondaryPalette() =>
      _secondaryPalette ??= MaterialPalette.accent.generate(secondary!.rgb);

  primaryVariant ??= primaryPalette() - 2;
  secondary ??= () {
    final palette = MaterialPalette.accent.generate(primary.rgb, variant: secondaryFromPrimary);
    _secondaryPalette = palette;
    return palette.swatch[palette.palette.primaryIndex];
  }();
  secondaryVariant ??= secondaryPalette() - 2;
  surface ??= background;
  onPrimary ??= primary.contrastColor(onDark: onDark, onLight: onLight);
  onSecondary ??= secondary.contrastColor(onDark: onDark, onLight: onLight);
  onSurface ??= surface.contrastColor(onDark: onDark, onLight: onLight);
  onBackground ??= background.contrastColor(onDark: onDark, onLight: onLight);
  onError ??= error.contrastColor(onDark: onDark, onLight: onLight);
  return ColorScheme(
    primary: primary,
    primaryVariant: primaryVariant,
    secondary: secondary,
    secondaryVariant: secondaryVariant,
    surface: surface,
    background: background,
    error: error,
    onPrimary: onPrimary,
    onSecondary: onSecondary,
    onSurface: onSurface,
    onBackground: onBackground,
    onError: onError,
    brightness: brightness,
  );
}

extension ContrastExt on Color {
  Color contrastColor({
    Color onDark = Colors.white,
    Color onLight = Colors.black,
  }) =>
      ThemeData.estimateBrightnessForColor(this) == Brightness.dark ? onDark : onLight;
}
