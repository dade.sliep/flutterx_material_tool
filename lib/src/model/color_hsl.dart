part of 'colors.dart';

extension HSLColorExt on HSLColor {
  RGBColor toRGBColor() => RGBColor.fromColor(toColor());

  HSLColor shift(double degrees) => withHue((hue + degrees).clampDegrees());
}
