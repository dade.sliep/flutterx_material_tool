part of 'colors.dart';

@immutable
class RGBColor {
  final double alpha;
  final double red;
  final double green;
  final double blue;

  factory RGBColor.fromColor(Color color) =>
      RGBColor.fromARGB(color.opacity, color.red / 255.0, color.green / 255.0, color.blue / 255.0);

  const RGBColor.fromARGB(this.alpha, this.red, this.green, this.blue)
      : assert(alpha >= 0.0 && alpha <= 1.0),
        assert(red >= 0.0 && red <= 1.0),
        assert(green >= 0.0 && green <= 1.0),
        assert(blue >= 0.0 && blue <= 1.0);

  Color toColor() => Color.fromRGBO((red * 255).round(), (green * 255).round(), (blue * 255).round(), alpha);

  HSLColor toHSLColor() => HSLColor.fromColor(toColor());

  LABColor toLABColor() {
    double x1(double v) => v > .04045 ? math.pow((v + .055) / 1.055, 2.4) as double : v / 12.92;

    double x2(a) {
      const b = 6 / 29;
      final c = 1 / (3 * math.pow(b, 2));
      return (a > math.pow(b, 3) ? math.pow(a, 1 / 3) : c * a + 4 / 29) as double;
    }

    final xR = x1(red);
    final xG = x1(green);
    final xB = x1(blue);
    final xE = .2126729 * xR + .7151522 * xG + .0721750 * xB;
    return LABColor.fromALAB(
        alpha,
        116 * x2(xE) - 16,
        500 * (x2((.4124564 * xR + .3575761 * xG + .1804375 * xB) / .95047) - x2(xE)),
        200 * (x2(xE) - x2((.0193339 * xR + .1191920 * xG + .9503041 * xB) / 1.08883)));
  }

  RGBColor shift(double degrees) => HSLColor.fromColor(toColor()).shift(degrees).toRGBColor();

  @override
  String toString() => '${objectRuntimeType(this, 'HSLColor')}($alpha, $red, $green, $blue)';

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is RGBColor && other.alpha == alpha && other.red == red && other.green == green && other.blue == blue;

  @override
  int get hashCode => hashValues(alpha, red, green, blue);
}
