import 'dart:math' as math;

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutterx_material_tool/src/utils.dart';

part 'color_hsl.dart';
part 'color_lab.dart';
part 'color_lch.dart';
part 'color_rgb.dart';

extension ColorExt on Color {
  RGBColor get rgb => RGBColor.fromColor(this);
}
