import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutterx_material_tool/flutterx_material_tool.dart';
import 'package:flutterx_material_tool/src/model/colors.dart';
import 'package:flutterx_material_tool/src/model/material_palette.dart';
import 'package:flutterx_material_tool/src/model/material_variant.dart';
import 'package:flutterx_material_tool/src/tool.dart';

@immutable
class PaletteData<T> {
  /// Source palette that generated this data
  final MaterialPalette<T> palette;

  /// Input color from which this data has been generated
  final RGBColor input;

  /// Index of [input] in generated [swatch]
  final int inputIndex;

  /// Generated palette colors DESC
  final List<Color> swatch;

  const PaletteData._(this.palette, this.input, this.inputIndex, this.swatch)
      : assert(inputIndex >= 0 && inputIndex < swatch.length);

  /// Generate palette data from [input], [palette] and [variant]
  factory PaletteData.from(RGBColor input, MaterialPalette<T> palette, MaterialVariant variant) {
    input = variant.apply(input);
    final swatch = generateSwatch(input, palette)
        .map((rgb) => rgb.toColor())
        .toList(growable: false)
        .reversed
        .toList(growable: false);
    return PaletteData._(palette, input, swatch.indexOf(input.toColor()), swatch);
  }

  /// Map this data into [T]
  T get data => palette.map(this);

  /// Get color at [inputIndex] - [delta]
  Color operator -(int delta) => swatch[(inputIndex - delta).clamp(0, swatch.length - 1)];

  /// Get color at [inputIndex] + [delta]
  Color operator +(int delta) => swatch[(inputIndex + delta).clamp(0, swatch.length - 1)];

  /// Generate [MaterialVariant.complementary] palette this [input]
  PaletteData get complementary => palette.generate(input, variant: MaterialVariant.complementary);

  /// Generate [MaterialVariant.analogous_a] palette this [input]
  PaletteData get analogousA => palette.generate(input, variant: MaterialVariant.analogous_a);

  /// Generate [MaterialVariant.analogous_b] palette this [input]
  PaletteData get analogousB => palette.generate(input, variant: MaterialVariant.analogous_b);

  /// Generate [MaterialVariant.triadic_a] palette this [input]
  PaletteData get triadicA => palette.generate(input, variant: MaterialVariant.triadic_a);

  /// Generate [MaterialVariant.triadic_b] palette this [input]
  PaletteData get triadicB => palette.generate(input, variant: MaterialVariant.triadic_b);
}
