part of 'colors.dart';

@immutable
class LABColor {
  final double alpha;
  final double lightness;
  final double a;
  final double b;

  const LABColor.fromALAB(this.alpha, this.lightness, this.a, this.b)
      : assert(alpha >= 0.0 && alpha <= 1.0),
        assert(lightness >= 0.0 && lightness <= 100.0),
        assert(a >= -128.0 && a <= 128.0),
        assert(b >= -128.0 && b <= 128.0);

  LCHColor toLCHColor() =>
      LCHColor.fromALCH(alpha, lightness, diagonal(a, b), degrees(math.atan2(b, a)).clampDegrees());

  @override
  String toString() => '${objectRuntimeType(this, 'LABColor')}($alpha, $lightness, $a, $b)';

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is LABColor && other.alpha == alpha && other.lightness == lightness && other.a == a && other.b == b;

  @override
  int get hashCode => hashValues(alpha, lightness, a, b);
}
