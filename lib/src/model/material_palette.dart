import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutterx_material_tool/src/model/colors.dart';
import 'package:flutterx_material_tool/src/model/material_variant.dart';
import 'package:flutterx_material_tool/src/model/palette_data.dart';

/// Create [T] from [primary] color and [swatch]
typedef SwatchMapper<T> = T Function(Color primary, Map<int, Color> swatch);

@immutable
class MaterialPalette<T> {
  /// Shades for this palette ASC
  final List<int> shades;

  /// List of palettes available in this collection
  final List<List<LABColor>> palettes;

  /// Index of primary shade in [shades]
  final int primaryShadeIndex;

  /// Converts swatch to [T]
  final SwatchMapper mapper;

  /// Index of primary color in generated [PaletteData.swatch]
  int get primaryIndex => shades.length - 1 - primaryShadeIndex;

  const MaterialPalette._(this.shades, this.primaryShadeIndex, this.palettes, this.mapper);

  factory MaterialPalette.create({
    required List<int> shades,
    required int primaryIndex,
    required List<ColorSwatch> swatches,
    required SwatchMapper mapSwatch,
  }) =>
      MaterialPalette._(
          shades,
          primaryIndex,
          swatches
              .map((swatch) =>
                  shades.map((shade) => RGBColor.fromColor(swatch[shade]!).toLABColor()).toList(growable: false))
              .toList(growable: false),
          mapSwatch);

  /// Create [T] from [data] using [mapper]
  T map(PaletteData data) {
    final swatch = <int, Color>{};
    final l = shades.length;
    for (var i = 0; i < l; i++) swatch[shades[l - 1 - i]] = data.swatch[i];
    return mapper(data.swatch[primaryIndex], swatch);
  }

  /// Generates [PaletteData] from [input]
  /// You can also specify a [variant] to shift input color
  PaletteData<T> generate(RGBColor input, {MaterialVariant variant = MaterialVariant.primary}) =>
      PaletteData<T>.from(input, this, variant);

  /// [MaterialColor] palette collection
  static final MaterialPalette<MaterialColor> primary = MaterialPalette<MaterialColor>.create(
      shades: const [50, 100, 200, 300, 400, 500, 600, 700, 800, 900],
      primaryIndex: 5,
      swatches: const <MaterialColor>[
        Colors.red,
        Colors.pink,
        Colors.purple,
        Colors.deepPurple,
        Colors.indigo,
        Colors.blue,
        Colors.lightBlue,
        Colors.cyan,
        Colors.teal,
        Colors.green,
        Colors.lightGreen,
        Colors.lime,
        Colors.yellow,
        Colors.amber,
        Colors.orange,
        Colors.deepOrange,
        Colors.brown,
        Colors.grey,
        Colors.blueGrey,
      ],
      mapSwatch: (primary, swatch) => MaterialColor(primary.value, swatch));

  /// [MaterialAccentColor] palette collection
  static final MaterialPalette<MaterialAccentColor> accent = MaterialPalette<MaterialAccentColor>.create(
      shades: const [100, 200, 400, 700],
      primaryIndex: 1,
      swatches: const <MaterialAccentColor>[
        Colors.redAccent,
        Colors.pinkAccent,
        Colors.purpleAccent,
        Colors.deepPurpleAccent,
        Colors.indigoAccent,
        Colors.blueAccent,
        Colors.lightBlueAccent,
        Colors.cyanAccent,
        Colors.tealAccent,
        Colors.greenAccent,
        Colors.lightGreenAccent,
        Colors.limeAccent,
        Colors.yellowAccent,
        Colors.amberAccent,
        Colors.orangeAccent,
        Colors.deepOrangeAccent,
      ],
      mapSwatch: (primary, swatch) => MaterialAccentColor(primary.value, swatch));
}
