import 'package:flutterx_material_tool/src/model/colors.dart';

/// Main palette variants
enum MaterialVariant {
  primary,
  complementary,
  analogous_a,
  analogous_b,
  triadic_a,
  triadic_b,
}

extension MaterialVariantExt on MaterialVariant {
  /// Hue delta to obtain variant from input color
  double get shift {
    switch (this) {
      case MaterialVariant.primary:
        return 0;
      case MaterialVariant.complementary:
        return 180;
      case MaterialVariant.analogous_a:
        return -30;
      case MaterialVariant.analogous_b:
        return 30;
      case MaterialVariant.triadic_a:
        return 60;
      case MaterialVariant.triadic_b:
        return 120;
    }
  }

  /// Get variant from [input]
  RGBColor apply(RGBColor input) {
    final s = shift;
    return s == 0 ? input : input.shift(s);
  }

  /// Get original input from [input] variant
  RGBColor remove(RGBColor input) {
    final s = shift;
    return s == 0 ? input : input.shift(-s);
  }
}
