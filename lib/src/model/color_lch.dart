part of 'colors.dart';

@immutable
class LCHColor {
  final double alpha;
  final double lightness;
  final double chroma;
  final double hue;

  const LCHColor.fromALCH(this.alpha, this.lightness, this.chroma, this.hue)
      : assert(alpha >= 0.0 && alpha <= 1.0),
        assert(lightness >= 0.0),
        assert(chroma >= 0.0),
        assert(hue >= 0.0 && hue <= 360);

  @override
  String toString() => '${objectRuntimeType(this, 'LCHColor')}($alpha, $lightness, $chroma, $hue)';

  LABColor toLABColor() {
    final dar = radians(hue);
    return LABColor.fromALAB(alpha, lightness, chroma * math.cos(dar), chroma * math.sin(dar));
  }

  RGBColor toRGBColor() {
    double x1(double a) {
      const b = 6 / 29;
      final c = 3 * math.pow(b, 2);
      return (a > b ? math.pow(a, 3) : c * (a - 4 / 29)) as double;
    }

    double x2(a) => .0031308 >= a ? 12.92 * a : 1.055 * math.pow(a, 1 / 2.4) - .055;

    final lab = toLABColor();
    final xE = (lab.lightness + 16) / 116;
    final xA = .95047 * x1(xE + lab.a / 500);
    final xL = x1(xE);
    final xB = 1.08883 * x1(xE - lab.b / 200);
    return RGBColor.fromARGB(
        lab.alpha,
        x2(3.2404542 * xA + -1.5371385 * xL + -.4985314 * xB).clamp(0, 1),
        x2(-.969266 * xA + 1.8760108 * xL + .041556 * xB).clamp(0, 1),
        x2(.0556434 * xA + -.2040259 * xL + 1.0572252 * xB).clamp(0, 1));
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is LCHColor &&
          other.alpha == alpha &&
          other.lightness == lightness &&
          other.chroma == chroma &&
          other.hue == hue;

  @override
  int get hashCode => hashValues(alpha, lightness, chroma, hue);
}
