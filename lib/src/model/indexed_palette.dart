import 'dart:math' as math;

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutterx_material_tool/src/model/colors.dart';
import 'package:flutterx_material_tool/src/utils.dart';

@immutable
class IndexedPalette {
  /// Index of input color in found [palette]
  final int index;

  /// Palette element of given palettes that contains a color close to the given one in [of] factory
  final List<LABColor> palette;

  const IndexedPalette._(this.index, this.palette)
      : assert(index >= 0),
        assert(palette.length > 0);

  /// Searches for the [palette] inside [palettes] that contains the color at [index] which is closest to the given one.
  factory IndexedPalette.of(LABColor color, List<List<LABColor>> palettes) {
    const epsilon = 1E-4;
    double distance(double a, double b) {
      if (a.abs() < epsilon && b.abs() < epsilon) return 0;
      return degrees(math.atan2(a, b)).clampDegrees();
    }

    var index = -1;
    var palette = palettes[0];
    var minDst = double.infinity;
    for (var i = 0; i < palettes.length; i++) {
      for (var j = 0; j < palettes[i].length && 0 < minDst; j++) {
        final current = palettes[i][j];
        final l = (current.lightness + color.lightness) / 2;
        var cd = diagonal(current.a, current.b);
        var sd = diagonal(color.a, color.b);
        var u = (cd + sd) / 2;
        u = .5 * (1 - math.sqrt(math.pow(u, 7) / (math.pow(u, 7) + math.pow(25, 7))));
        var cu = current.a * (1 + u);
        var su = color.a * (1 + u);
        var cdu = diagonal(cu, current.b);
        var sdu = diagonal(su, color.b);
        final delta = sdu - cdu;
        final deltaAvg = (cdu + sdu) / 2;
        cu = distance(current.b, cu);
        su = distance(color.b, su);
        cdu = 2 *
            math.sqrt(cdu * sdu) *
            math.sin((epsilon > cd.abs() || epsilon > sd.abs()
                    ? 0
                    : 180 >= (su - cu).abs()
                        ? su - cu
                        : su <= cu
                            ? su - cu + 360
                            : su - cu - 360) /
                2 *
                degrees2Radians);
        cd = epsilon > cd.abs() || epsilon > sd.abs()
            ? 0
            : 180 >= (su - cu).abs()
                ? (cu + su) / 2
                : 360 > cu + su
                    ? (cu + su + 360) / 2
                    : (cu + su - 360) / 2;
        sd = 1 + .045 * deltaAvg;
        sdu = 1 +
            .015 *
                deltaAvg *
                (1 -
                    .17 * math.cos((cd - 30) * degrees2Radians) +
                    .24 * math.cos(2 * cd * degrees2Radians) +
                    .32 * math.cos((3 * cd + 6) * degrees2Radians) -
                    .2 * math.cos((4 * cd - 63) * degrees2Radians));
        final dist = math.sqrt(math.pow(
                (color.lightness - current.lightness) /
                    (1 + .015 * math.pow(l - 50, 2) / math.sqrt(20 + math.pow(l - 50, 2))),
                2) +
            math.pow(delta / (1 * sd), 2) +
            math.pow(cdu / (1 * sdu), 2) +
            delta /
                sd *
                math.sqrt(math.pow(deltaAvg, 7) / (math.pow(deltaAvg, 7) + math.pow(25, 7))) *
                math.sin(60 * math.exp(-math.pow((cd - 275) / 25, 2)) * degrees2Radians) *
                -2 *
                (cdu / sdu));
        if (dist < minDst) {
          minDst = dist;
          palette = palettes[i];
          index = j;
        }
      }
    }
    return IndexedPalette._(index, palette);
  }

  @override
  String toString() => 'IndexedPalette(index: $index, palette: [${palette.join(', ')}])';

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is IndexedPalette && other.index == index && listEquals(other.palette, palette);

  @override
  int get hashCode => hashValues(index, hashList(palette));
}
