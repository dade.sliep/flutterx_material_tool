import 'dart:math' as math;

double diagonal(double a, double b) => math.sqrt(math.pow(a, 2) + math.pow(b, 2));

extension DoubleExt on double {
  double clampDegrees() => (this + 360) % 360;
}

double degrees(double radians) => radians * radians2Degrees;

double radians(double degrees) => degrees * degrees2Radians;

const double degrees2Radians = math.pi / 180.0;
const double radians2Degrees = 180.0 / math.pi;
